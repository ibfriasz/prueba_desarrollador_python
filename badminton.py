rallyA = 0
rallyB = 0
punto = ''
setActual = 1
setPointA = False
setPointB = False
cambioCampo = False
setsGanadosA = 0
setsGanadosB = 0

print 'Escriba las letras A o B entre comillas simples o sencillas para indicar un punto a favor de cada jugador o equipo, para salir de manera anticipada intruduzca "e"'

def mensajeError():
	print 'Entrada invalida, favor de introducir "A" o "B"'

def setPoint(equipo):
	global setPointA
	global setPointB
	global matchPointA
	global matchPointB

	if (equipo == 'A' and setsGanadosA == 0) or (equipo == 'B' and setsGanadosB == 0):
		print 'Set Point para ' + equipo
		if equipo == 'A':
			setPointA = True
		elif equipo == 'B':
			setPointB = True

	elif (equipo == 'A' and setsGanadosA == 1) or (equipo == 'B' and setsGanadosB == 1):
		print 'Match Point para ' + equipo
		if equipo == 'A':
			setPointA = True
		elif equipo == 'B':
			setPointB = True

def resetValues():
	global rallyA
	global rallyB
	global setPointA
	global setPointB
	rallyA = 0
	rallyB = 0
	setPointA = False
	setPointB = False

def marcadorGlobal(equipo):
	global setsGanadosA
	global setsGanadosB
	if equipo == 'A':
		setsGanadosA += 1
	else:
		setsGanadosB += 1

	if setsGanadosA < 2 and setsGanadosB < 2:
		print 'Marcador Global ' + str(setsGanadosA) + '  ' + str(setsGanadosB)
		print 'Cambio de campo'
		resetValues()
	else:
		print 'Marcador Final ' + str(setsGanadosA) + '  ' + str(setsGanadosB)
		#print 'Cambio de campo'
		resetValues()

	if setsGanadosA == 2:
		print 'El Ganador es A'
		exit()
	elif setsGanadosB ==2:
		print 'El Ganador es B'
		exit()


while True:
	while punto != 'A' or punto != 'B' or punto != 'E':

		try:
			punto=input()
			break

		except:
			mensajeError()

	if punto == 'A' or punto == 'a':
		rallyA += 1

	elif punto == 'B' or punto == 'b':
		rallyB += 1

	elif punto == 'E' or punto == 'e':
		exit()
		
	else:
		mensajeError()

	if ((rallyA == 11 and rallyB < rallyA) or (rallyB == 11 and rallyA < rallyB)) and setsGanadosA==1 and setsGanadosB==1:
		print 'Cambio de campo'

	#print setPointA
	#print setPointB
	if (setPointA == True and (punto == 'A' or punto == 'a')) or rallyA == 30:
		marcadorGlobal('A')
		#print 'entre en marcador'
	elif (setPointB == True and (punto == 'B' or punto == 'b')) or rallyB == 30:
		marcadorGlobal('B')
		#print 'entre en el otro marcador'

	if rallyA == rallyB:
		setPointA = False
		setPointB = False
		matchPointA = False
		matchPointB = False

	print 'La puntuacion es ' + str(rallyA) + '  ' + str(rallyB)
	print '\n'

	if rallyA == 20 and rallyB < 20:
		setPoint('A')
	elif rallyB == 20 and rallyA < 20:
		setPoint('B')
	if rallyB > 19 and rallyA > rallyB:
		setPoint('A')
		#print 'entrando mayor 20'
	elif rallyA > 19 and rallyB > rallyA:
		setPoint('B')
		#print 'entrando mayor 20 B'
	if rallyA == 29:
		setPoint('A')
	if rallyB == 29:
		setPoint('B')

	punto = ''
