# -*- coding: utf-8 -*-
import sys

print 'Introduzca una cadena de texto iniciando y terminando con comillas simples o dobles'

try:
	texto = input()

except:
	print 'Entrada inválida, saliendo del programa'
	exit()

contador = 0
texto2 = []

print sys.stdout.encoding

for letra in texto:
	if letra == 'a' or letra == 'e' or letra == 'i' or letra == 'o' or letra == 'u':
		contador += 1
		letra = 'æ'
	elif letra == 'A' or letra == 'E' or letra == 'I' or letra == 'O' or letra == 'U':
		contador += 1
		letra = 'Æ'
	texto2.append(letra)

texto2 = ''.join(texto2)

print('El numero de vocales es: ')
print(contador)
print('El texto es el siguiente: ')
print(texto2)
